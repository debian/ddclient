Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ddclient
Upstream-Contact: https://github.com/ddclient/ddclient
Source: https://ddclient.net

Files: *
Copyright:
 2006-2014  wimpunk <wimpunk+github@gmail.com>
 1999-2001  Paul Burry (pburry@canada.com)
License: GPL-2.0+

Files: debian/*
Copyright:
 2020-2024  Richard Hansen <rhansen@rhansen.org>
 2005-2016  Torsten Landschoff <torsten@debian.org>
 2014  Tong Sun <suntong@cpan.org>
 2013  Teemu Ikonen <tpikonen@gmail.com>
 2013  Antonio Terceiro <terceiro@debian.org>
 2005-2012  Christian Perrier <bubulle@debian.org>
 2012  Stefano Rivera <stefanor@debian.org>
 2008  Tobias Toedter <toddy@debian.org>
 2007  Marco Rodrigues <gothicx@sapo.pt>
 2002  Felix Kröger <felix.kroeger@gmx.de>
 2000  Steve Greenland <stevegr@debian.org>
License: GPL-2.0+

Files: debian/po/*
Copyright:
 2021  Américo Monteiro <a_monteiro@gmx.com>
 2020  Ari Ervasti <ari.ervasti87@gmail.com>
 2020  Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>
 2020  Adriano Rafael Gomes <adrianorg@debian.org>
 2020  José Vieira <jvieira33@sapo.pt>
 2020  Sebastian Rasmussen <sebras@gmail.com>
 2020  Rui Branco <ruipb@debianpt.org>
 2020  Takuma Yamada <tyamada@takumayamada.com>
 2020  Richard Hansen <rhansen@rhansen.org>
 2014, 2020  Frans Spiesschaert <Frans.Spiesschaert@yucom.be>
 2014  Miroslav Kure <kurem@debian.cz>
 2014  Kenshi Muto <kmuto@debian.org>
 2014  Olexandr Kravchuk <sashko.debian@gmail.com>
 2014  Asho Yeh <asho@debian.org.tw>
 2014  traducción de Debian al español (<debian-l10n-spanish@lists.debian.org>)
 2012  Michał Kułach <michal.kulach@gmail.com>
 2011  Jeroen Schot <schot@a-eskwadraat.nl>
 2011  Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
 2010  Korrekturlæst "Ask Hjorth Larsen" <asklarsen@gmail.com>
 2010  Joe Hansen <joedalton2@yahoo.dk>
 2010  Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>
 2010  Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>
 2010  Bart Cornelis <cobaco@skolelinux.no>
 2010  Martin Bagge <brother@bsnet.se>
 2009  marce villarino <mvillarino@users.sourceforge.net>
 2009  Esko Arajärvi <edu@iki.fi>
 2009  Bjørn Steensrud <bsteensr@skogkatt.homelinux.org>
 2009, 2010, 2020  Yuri Kozlov <yuray@komyakino.ru>
 2009, 2010  Miguel Figueiredo <elmig@debianpt.org>
 2009, 2010  Martin Bagge <brother@bsnet.se>
 2009, 2010  Ivan Masár <helix84@centrum.sk>
 2009, 2010  Flamarion Jorge <jorge.flamarion@gmail.com>
 2008  Yuri Kozlov <kozlov.y@gmail.com>
 2008, 2009  Piarres Beobide <pi@beobide.net>
 2008, 2009, 2010  Vincenzo Campanella <vinz65@gmail.com>
 2007  Jacobo Tarrio <jtarrio@debian.org>
 2006  Ricardo Silva <ardoric@gmail.com>
 2006, 2009  Jordà Polo <jorda@ettin.org>
 2006, 2007, 2009, 2020  Helge Kreutzmann <debian@helgefjell.de>
 2005-2010  Clytie Siddall <clytie@riverland.net.au>
 2005-2009  Debian French l10n team <debian-l10n-french@lists.debian.org>
 2005, 2009, 2010  Christian Perrier <bubulle@debian.org>
 2003  Vicente H. <vherrerv@supercable.es>
 2002  Felix Kröger <felix.kroeger@gmx.de>
License: GPL-2.0+

Files: t/lib/Devel/Autoflush.pm
Copyright: 2014 David Golden
License: Apache-2.0

Files:
 t/lib/Test/*
 t/lib/Test2.pm
 t/lib/Test2/*
 t/lib/ok.pm
Copyright: 2019 Chad Granum <exodist@cpan.org>
License: Artistic or GPL-1+

Files: t/lib/ddclient/Test/Fake/HTTPD.pm
Copyright: 2011-2017 NAKAGAWA Masaki <masaki@cpan.org>
License: Artistic or GPL-1+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of version 2.0 of the Apache
 License can be found in "/usr/share/common-licenses/Apache-2.0".

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in "/usr/share/common-licenses/Artistic".

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-1".
